/**
 * Created by MarcoC on 02/01/2017.
 */
export class Video {
  id: number;
  name: string;
  url: string;
  start: number;
  end: number;
}
