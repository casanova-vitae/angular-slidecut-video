/**
 * Created by MarcoC on 02/01/2017.
 */
import { Component } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'my-dashboard',
  templateUrl: 'dashboard.component.html',

})
export class DashboardComponent { }
