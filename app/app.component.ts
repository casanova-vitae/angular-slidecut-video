import { Component } from '@angular/core';
import { Video } from './video';
const VIDEOS: Video[] = [
  { id: 1, name: 'Vid Start', url: 'http://grochtdreis.de/fuer-jsfiddle/video/sintel_trailer-480.mp4', start: 2, end: 4 },
  { id: 2, name: 'Vid Start', url: 'http://grochtdreis.de/fuer-jsfiddle/video/sintel_trailer-480.mp4', start: 4, end: 5 },
  { id: 3, name: 'Vid Start', url: 'http://grochtdreis.de/fuer-jsfiddle/video/sintel_trailer-480.mp4', start: 7, end: 9 },
  { id: 4, name: 'Vid Start', url: 'http://grochtdreis.de/fuer-jsfiddle/video/sintel_trailer-480.mp4', start: 11, end: 12 },
  { id: 5, name: 'Vid Start', url: 'http://grochtdreis.de/fuer-jsfiddle/video/sintel_trailer-480.mp4', start: 14, end: 18 }
];
@Component({
  selector: 'my-app',
  template: `
    <h1>{{title}}</h1>
    <h2>Video Clip Creator</h2>
     <table style="width:100%">
      <tr>
        <th>
          <ul class="videos">
            <li *ngFor="let video of videos"
              [class.selected]="video === selectedVideo"
              (click)="onSelect(video)">
              <span class="badge">{{video.id}}</span> {{video.name}}
            </li>
          </ul>
        </th>
        <th>
          <my-video-detail [video]="selectedVideo"></my-video-detail>
        </th>
      </tr>
    </table>
    
  `,
  styles: [`
    .selected {
      background-color: #CFD8DC !important;
      color: white;
    }
    .videos {
      margin: 0 0 2em 0;
      list-style-type: none;
      padding: 0;
      width: 15em;
    }
    .videos li {
      cursor: pointer;
      position: relative;
      left: 0;
      background-color: #EEE;
      margin: .5em;
      padding: .3em 0;
      height: 2.6em;
      border-radius: 4px;
    }
    .videos li.selected:hover {
      background-color: #BBD8DC !important;
      color: white;
    }
    .videos li:hover {
      color: #607D8B;
      background-color: #DDD;
      left: .1em;
    }
    .videos .text {
      position: relative;
      top: -3px;
    }
    .videos .badge {
      display: inline-block;
      font-size: small;
      color: white;
      padding: 0.8em 0.7em 0 0.7em;
      background-color: #607D8B;
      line-height: 1em;
      position: relative;
      left: -1px;
      top: -4px;
      height: 1.8em;
      margin-right: .8em;
      border-radius: 4px 0 0 4px;
    }
  `]
})
export class AppComponent {
  title = 'Tour of videos';
  videos = VIDEOS;
  selectedVideo: Video;
  onSelect(video: Video): void {
    this.selectedVideo = video;
  }
}
