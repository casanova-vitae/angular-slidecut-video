/**
 * Created by MarcoC on 02/01/2017.
 */
import { Component, Input } from '@angular/core';
import { Video } from './video';
@Component({
  selector: 'my-video-detail',
  template: `
    <div *ngIf="video">
      <h2>{{video.name}} details!</h2>
      <div><label>id: </label>{{video.id}}</div>
      <div>
        <label>name: </label>
        <input [(ngModel)]="video.name" placeholder="name"/>
        <label>url: </label>
        <input [(ngModel)]="video.url" placeholder="name"/>
        <label>time start: </label>
        <input [(ngModel)]="video.start" placeholder="name"/>
        <label>time end: </label>
        <input [(ngModel)]="video.end" placeholder="name"/><br>
        <video id="frag1" controls preload="metadata" width="220px" height="220px">
            <source src = "{{video.url}}#t={{video.ini}},{{video.end}}">
        </video>

      </div>
    </div>
  `
})
export class VideoDetailComponent {
  @Input()
  video : Video;
}

